#!/bin/bash
gcloud compute firewall-rules create  puma-default\
 --allow tcp:80 \
 --target-tags=docker-machine \
 --description=" monitoring dss connections" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:8080 \
 --target-tags=docker-machine \
 --description=" cAdvisor" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:9090 \
 --target-tags=docker-machine \
 --description="Prometheus" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:3000 \
 --target-tags=docker-machine \
 --description="Grafana" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:8787 \
 --target-tags=docker-machine \
 --description="DSS" \
 --direction=INGRESS
