# Инфраструктура:
## 1. Для сборки приложения используются штатные средства предоставляемые gitlab.com

Ресурсы GCP. В качестве инструмента для подготовки виртуальных машин используется docker-machine.
Представляет собой две виртуальную машину со следующим функционалом:
## 2.	Dss – сервер приложения + сервер мониторинга
 	
 	Создание виртуальной машины dss скриптом  create_dss.sh: 
```
#!/bin/bash
export GOOGLE_PROJECT=docker-240808
docker-machine create --driver google \
    --google-machine-image https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1604-lts \
    --google-machine-type n1-standard-1 \
    --google-open-port 9090/tcp \
    --google-open-port 8787/tcp \
    --google-open-port 8080/tcp \
    --google-open-port 3000/tcp \
    dss
```
   Создание правила firewall скриптом create_firewall_rules_dss.sh
```
#!/bin/bash
gcloud compute firewall-rules create  puma-default\
 --allow tcp:80 \
 --target-tags=docker-machine \
 --description=" monitoring dss connections" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:8080 \
 --target-tags=docker-machine \
 --description=" cAdvisor" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:9090 \
 --target-tags=docker-machine \
 --description="Prometheus" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:3000 \
 --target-tags=docker-machine \
 --description="Grafana" \
 --direction=INGRESS

gcloud compute firewall-rules create  puma-default\
 --allow tcp:8787 \
 --target-tags=docker-machine \
 --description="DSS" \
 --direction=INGRESS

```

# Запуск приложения осуществлятеся с использоватнием docker-compose.yml и сервера мониторинга при помощи docker-compose-monitoring.yml соответственно.


# Приложение:

## Предназначение:
Подсистема расчета признаков и рекомендаций
## Архитектура:
![](dss_arch.png)
В качестве базы данных используется MongoDB

## Возможности:
- Управление данными пациентов
- Управление правилами расчета признаков и рекомендаций
- Расчёт признаков
- Расчёт рекомендаций

## API Reference:

### Patient
- GET /patients Коллекция пациентов
- POST /patients Создание пациента
- GET /patients/{patientGUID} Информация о пациенте
- PATCH /patients/{patientGUID} Изменение пациента
- DELETE /patients/{patientGUID} Удаление пациента
- GET /patients/{patientGUID}/params Коллекция параметров пациента
- POST /patients/{patientGUID}/params Добавление параметра пациенту
- PUT /patients/{patientGUID}/params/{paramGUID} Изменение параметра пациента
- DELETE /patients/{patientGUID}/params/{paramGUID} Удаление параметра пациента
- GET /patients/{patientGUID}/recommendations Получение рекоммендаций для пациента Symptom

### Symptoms
- GET /symptoms Коллекция правил признаков
- POST /symptoms Создание правила признака
- GET /symptoms/{symptomCode} Информация о правиле признака
- PATCH /symptoms/{symptomCode} Изменение правила признака
- DELETE /symptoms/{symptomCode} Удаление правила признака

### Recommendation
- GET /recommendations оллекция правил рекоммендаций
- POST /recommendations Создание правила рекоммендации
- GET /recommendations/{recommendationCode} Информация о правиле рекоммендации
- PATCH /recommendations/{recommendationCode}Изменение правила рекоммендации
- DELETE /recommendations/{recommendationCode} Удаление правила рекоммендации

## Параметры запуска
static -c [путь до файла конфигурации]

## Конфигурация
* - обязательные параметры

_- значения по умолчанию

```
--------------------------------------------------------------------------------------------------------------------------------
| Параметр			  | Тип		| Описание													                           | VERSION 	| 	ENV					            | 
--------------------------------------------------------------------------------------------------------------------------------|
| http.host			  | string	| Адрес бинд-хоста HTTP интерфейса (0.0.0.0)	               | 0.1+		  |	DSS_HTTP_HOST		          |
| http.port			  | integer	| Порт бинд-хоста HTTP интерфейса (80)				               | 0.1+		  |	DSS_HTTP_PORT		          |
| *db.host			  | string	| Адрес хоста БД											                       | 0.1+		  |	DSS_DB_HOST			          |
| db.port			    | integer	| Порт хоста БД (27017)										                   | 0.1+		  |	DSS_DB_PORT			          |	
| db.login			  | string	| Логин пользователя БД										                   | 0.1+		  |	DSS_DB_LOGIN		          |
| db.password     | string	| Пароль пользователя БД									                   | 0.1+		  |	DSS_DB_PASSWORD		        |
| *db.database    | string	| Название БД												                         | 0.1+		  |	DSS_DB_DATABASE		        |
| prometheus.port | integer	| Порт метрик (9090)										                     | 0.2+		  |	DSS_PROMETHEUS_PORT	      |
| prometheus.path | string	| Путь до метрик (/metrics)									                 | 0.2+		  |	DSS_PROMETHEUS_PATH	      |
| sentryDSN	  		| string	| DSN аггрегатора ошибок									                   | 0.1+		  |	DSS_SENTRY_DSN   	        |
| logging.output 	| string	| Вывод ошибок в STDOUT или файл (указывается путь до файла) | 0.1+		  |	DSS_LOGGING_OUTPUT	      |
| logging.level		| string	| Уровень логирования (DEBUG|ERROR|FATAL|INFO)				       | 0.1+		  |	DSS_LOGGING_LEVEL	        |
| logging.format 	| string	| Формат логов (TEXT|JSON)									                 | 0.1+		  |	DSS_LOGGING_FORMAT	      |
| mq.host 			  | string	| Адрес брокера сооблщений									                 | 0.3+		  |	DSS_MQ_HOST			          |
| mq.port 			  | integer	| Порт брокера сообщений									                   | 0.3+		  |	DSS_MQ_PORT			          |
| mq.clientID 		| string	| Идентификатор клиента										                   | 0.3+		  |	DSS_MQ_CLIENTID		        |
| mq.login 			  | string	| Логин пользователя										                     | 0.3+		  |	DSS_MQ_LOGIN		          |
| mq.password 		| string	| Пароль пользователя										                     | 0.3+		  |	DSS_MQ_PASSWORD		        |
| mq.topic 			  | string	| Название очереди сообщений								                 | 0.3+		  |	DSS_MQ_TOPIC		          |
--------------------------------------------------------------------------------------------------------------------------------|
```
## Пример работы с приложением:
 0. Скачать пример коллекции для POSTMAN - OTUS_DSS.postman_collection.json
 1. Далее на первом этапе создаем в базе пациента 
 ```
pm.environment.set("patientGUID", pm.environment.get("randomGUID"));
pm.environment.set("patientBirthDate", "1990-01-30");
pm.environment.set("patientGenderCode", "MALE");
pm.environment.set("patientCKDCode", "5D");
```
 2. Создаем правила длz рекомендаций по лечению 
```
pm.environment.set("recommendationCode", pm.environment.get("randomGUID"));
pm.environment.set("recommendationDescription", "test_desc1");
pm.environment.set("recommendationCondition", JSON.stringify({"and":[{">":[{"var":"patient.birthDate"},"1990-01-01"]},{"==":[{"var":"patient.genderCode"},"MALE"]}]}));
```
 3. Расчет рекомендаций 
```
var jsonData = JSON.parse(responseBody)

pm.test("Success response is correct", function () {
    pm.expect(pm.response).to.have.property('code', 200);
    pm.expect(pm.response).to.have.property('status', 'OK');
    pm.expect(pm.response).to.have.header('Content-type', 'application/json');
});

pm.test('Base schema is valid', function() {
  pm.expect(tv4.validate(jsonData, glFn.getEnvSchema("baseSchema"))).to.be.true;
});

pm.test('Message data is valid', function() {
  pm.expect(jsonData).to.have.property("message", "SUCCESS");
});

pm.test('Entity schema is valid', function() {
  pm.expect(tv4.validate(jsonData.data[0].criteria, glFn.getEnvSchema("criteriaSchema"))).to.be.true;
});

pm.test("Entity data is correct", function () {
    pm.expect(jsonData.data[0].criteria).to.have.property("birthDate", pm.environment.get("patientBirthDate"));
    pm.expect(jsonData.data[0].criteria).to.have.property("genderCode", pm.environment.get("patientGenderCode"));
    pm.expect(jsonData.data[0].criteria).to.have.property("ckdCode", pm.environment.get("patientCKDCode"));
    pm.expect(jsonData.data[0].criteria).to.have.property("params", null);
});
```
# Пример работы можно посмотерть на видео 
Video_2019-08-12_162146.wmv
